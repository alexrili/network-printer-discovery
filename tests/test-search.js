
const ndp = require('../lib/network-printer-discovery');

const discovery = ndp({time_out:5000,allowed_ip_addresses:{starts:"192.168.0.45",ends:"192.168.0.55"}});

discovery.printerIpAddresses(function (printers) {
    console.log(printers);
    return printers;
});