;(function(){
    'use strict';

    // import all external libraries
    const os            = require( 'os' )
        , ipFromRange   = require('../custom_modules/ip-from-range')
        , _             = require('underscore')
        , portscanner   = require('portscanner')
        , sync          = require('synchronize');

    // create a new object and holden 'this' for global context
    let NetworkPrinterDiscovery = function (options) {
        return new NetworkPrinterDiscovery.init(options);
    };

    // nPD is a alias for (NetworkPrinterDiscovery);
    // nPD or NetworkPrinterDiscovery.fn is a collection of all function of this library
    // this means is a prototype
    let nPD = NetworkPrinterDiscovery.fn = NetworkPrinterDiscovery.prototype =  {};

    // return a array of all ip address of a range of ip, except the denied ip address
    nPD.printerIpAddresses = function(callback) {
        let _allowedIpAddresses = _getAllowedIpAddresses(this.allowed_ip_addresses, this.denied_ip_addresses);
        return _searchForPrinters(_allowedIpAddresses, this.port_number, this.time_out, function (printers) {
            return callback(printers);
        });
    };

    // search fro printers throw the array of ip addresses
    // must be recive the array of ip and a port for check
    const _searchForPrinters = function(allowedIpAddresses,port,time_out, callback){
        //for make the _checkPort function await until be ready
        sync.fiber(function () {

            //if not have the array of ip, should be stoped right now and return's a empty array
            if (!allowedIpAddresses) {
                return [];
            }

            // check all ipAddress and filter for open ports if they are open is a printer
            let _printers = _.filter(allowedIpAddresses, function (ip) {
                let status = sync.await(_checkPort(ip, port, time_out, sync.defer()));
                if(status == 'open'){
                    return ip;
                }else{
                    return false;
                }
            });
            callback(_printers);
        });
    };

    // check if port number is open
    // returns status = 'open' or 'closed'
    const _checkPort = function (ip,port,time_out, callback) {
        portscanner.checkPortStatus(port, {host:ip, time_out:time_out}, function(error, status) {
            return callback(null,status);
        });
    };

    // get a range of allowed ip address
    const _getAllowedIpAddresses = function (allowedRange,deniedIpAddress) {

        // if we have not this two parameters, the process must be stoped right now;
        // because we need them for continue
        if(!allowedRange.starts && !allowedRange.ends){
            throw "Error: must be a stars and ends {starts, ends})";
        }

        let _allIpAddressFromRange = ipFromRange(allowedRange.starts,allowedRange.ends).getIpAddresses();
        let allowedIpAddresses = _allIpAddressFromRange;

        // if have a any dined ip address filters throw
        // the array off ip and returns just the allowed's
        if(deniedIpAddress){
            allowedIpAddresses = _.reject(_allIpAddressFromRange,function (ipAddress) {
                return _.find(deniedIpAddress,function (deniedIp) {
                    return ipAddress === deniedIp;
                });
            });
        }
        return allowedIpAddresses;
    };

    // this is a private method
    // return's the range of a current local ip address
    const _getCurrentIpAddressRange = function(){

        //check for the current network
        const networkInterfaces = os.networkInterfaces( );

        // get the first parameter os ethernet property
        // and turn the ip address in one array
        let _currentIpAddress = networkInterfaces.eth0[0].address.split(".");

        // replace the last argument for a number '1'
        _currentIpAddress.splice(3,1,'1');
        // this recive the start ip address 'x.x.x.1'
        const _ipStarts    = String(_currentIpAddress).replace(/,/g,'.');

        // replace the last argument again for a number '255'
        _currentIpAddress.splice(3,1,'255');
        // this recive the end ip address 'x.x.x.255'
        const _ipEnds    = String(_currentIpAddress).replace(/,/g,'.');

        return {
            starts: _ipStarts,
            ends: _ipEnds
        };
    };

    // the actual object is created here, allowing us to 'new' an object without calling 'new'
    NetworkPrinterDiscovery.init = function (options){

        let self = this;

        // create the new object if nothing has be passed
        options = options || {};

        // default port number for any network printer
        self.port_number = options.port_number || 515;

        // sets timeout shorts as possible
        self.time_out = options.time_out || 300;

        // if not pass any range this will be provider the current range of ip of this local machine
        self.allowed_ip_addresses   = options.allowed_ip_addresses  || _getCurrentIpAddressRange();

        // this argument can be null
        self.denied_ip_addresses    = options.denied_ip_addresses   || null;

        return self;
    };

    // trick borrowed code, so we don't have to use the 'new' keyword
    NetworkPrinterDiscovery.init.prototype = NetworkPrinterDiscovery.fn;
    
    // exports a IpFromRange function
    return module.exports = NetworkPrinterDiscovery;

})();